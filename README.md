# moarbell
A door bell for the Physical Web

# Setting Up the Server

The server lives in the `server/` directory.  To set up the server, run the
following steps:

```bash
cd server/

# set up your virtual environment
virtualenv venv-moarbell

# activate the virtual environment
venv-moarbell/bin/activate

# install dependencies
pip install -r requirements.txt

# run the server
./app.py
```

By default, the server listens on `0.0.0.0:8888`.

# Credits

## The Doorbell Sound

The [Doorbell Sound](http://soundbible.com/165-Door-Bell.html) is recorded by
Mike Koenig and available under the Creative Commons [Attribution
3.0](https://creativecommons.org/licenses/by/3.0/us/) License.

## PeerJS

Copyright (c) 2015 Michelle Bu and Eric Zhang, http://peerjs.com

(The MIT License)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
