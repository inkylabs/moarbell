#!/usr/bin/env python

from flask import Flask
from blueprints import index
import random

app = Flask(__name__)

with app.app_context():
    app.server_rand = str(random.getrandbits(128))

app.register_blueprint(index.index_blueprint)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8888)
