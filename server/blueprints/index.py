from flask import Blueprint, request, render_template, current_app

index_blueprint = Blueprint('index', __name__)

@index_blueprint.route('/call', methods=['GET'])
def call():
    ''' Answer a call '''
    user_ip = request.remote_addr

    template = 'call.html'
    context = {
        'id_addr': user_ip,
        'server_rand': current_app.server_rand
    }
    return render_template(template, **context)

@index_blueprint.route('/receive', methods=['GET'])
def receive():
    ''' Receive a call '''
    user_ip = request.remote_addr

    template = 'receive.html'
    context = {
        'ip_addr': user_ip,
        'server_rand': current_app.server_rand
    }
    return render_template(template, **context)

@index_blueprint.route('/static/<path:path>', methods=['GET'])
def static(path):
    return flask.send_from_directory('static', path)
